module org.but.feec.javafx {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.slf4j;
    requires com.zaxxer.hikari;
    requires java.sql;
    requires bcrypt;
    requires org.controlsfx.controls;



    opens org.but.feec.javafx to javafx.fxml;
    exports org.but.feec.javafx;
    opens org.but.feec.javafx.controller to javafx.fxml;
    exports org.but.feec.javafx.controller;
}