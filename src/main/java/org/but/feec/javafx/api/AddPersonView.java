package org.but.feec.javafx.api;

public class AddPersonView {

    private Long id;
    private String name;
    private String surname;
    private int age;
    private String email;
    private int testResultID;


    public Long getId() {return id;}

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {this.email = email;}

    public String getName() {
        return name;
    }

    public void setName(String name) {this.name = name;}

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {return age;}

    public void setAge(int age) {
        this.age = age;
    }

    public int getResultID() {return testResultID;}

    public void setResultID(int testResultID) {this.testResultID = testResultID;}

}
