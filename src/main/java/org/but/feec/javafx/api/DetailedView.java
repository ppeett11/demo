package org.but.feec.javafx.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DetailedView {

    private LongProperty id = new SimpleLongProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty surname = new SimpleStringProperty();
    private StringProperty age = new SimpleStringProperty();
    private StringProperty result = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();
    private StringProperty city = new SimpleStringProperty();
    private StringProperty resultID = new SimpleStringProperty();


    public Long getId() {
        return idProperty().get();
    }

    public void setId(Long id) {
        this.idProperty().setValue(id);
    }

    public String getEmail() {
        return emailProperty().get();
    }

    public void setEmail(String email) {
        this.emailProperty().setValue(email);
    }

    public String getName() {return nameProperty().get();}

    public void setName(String givenName) {
        this.nameProperty().setValue(givenName);
    }

    public String getSurname() {
        return surnameProperty().get();
    }

    public void setSurname(String familyName) {
        this.surnameProperty().setValue(familyName);
    }

    public String getCity() {
        return cityProperty().get();
    }

    public void setCity(String city) {
        this.cityProperty().setValue(city);
    }

    public String getResult() {
        return resultProperty().get();
    }

    public void setResult(String result) {this.resultProperty().setValue(result);}

    public String getAge() {
        return ageProperty().get();
    }

    public void setAge(String age) {
        this.ageProperty().setValue(age);
    }

    public String getResultID() {return resultIDProperty().get();}

    public void setResultID(String resultID) {
        this.resultIDProperty().setValue(resultID);
    }

    public LongProperty idProperty() {
        return id;
    }

    public StringProperty emailProperty() {
        return email;
    }

    public StringProperty nameProperty() {return name;}

    public StringProperty surnameProperty() {
        return surname;
    }

    public StringProperty cityProperty() {
        return city;
    }

    public StringProperty resultProperty() {
        return result;
    }

    public StringProperty resultIDProperty() {
        return resultID;
    }

    public StringProperty ageProperty() {return age;}


}
