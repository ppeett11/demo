package org.but.feec.javafx.service;

import org.but.feec.javafx.api.AddPersonView;
import org.but.feec.javafx.api.DetailedView;
import org.but.feec.javafx.api.EditView;
import org.but.feec.javafx.api.PersonBasicView;
import org.but.feec.javafx.data.PersonRepository;

import java.util.List;

public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    public PersonService(){};

    public List<PersonBasicView> getPersonBasicView(){return personRepository.getPersonBasicView();}

    public DetailedView getDetailedView(Long id) {return personRepository.findPersonDetailedView(id);}

    public void editPerson(EditView editView) {personRepository.editPerson(editView);}

    public void addPerson(AddPersonView addPersonView) {personRepository.addPerson(addPersonView);}

}
