package org.but.feec.javafx.data;

import javafx.stage.Stage;
import org.but.feec.javafx.api.*;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exception.DataAccessException;
import org.but.feec.javafx.service.PersonService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Long.parseLong;

public class PersonRepository {

    public PersonAuthView findPersonByUserName(String user_name) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT user_name, user_password" +
                             " FROM mydb.users u" +
                             " WHERE u.user_name = ?")
        ) {
            preparedStatement.setString(1, user_name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    private PersonAuthView mapToPersonAuth(ResultSet rs) throws SQLException {
        PersonAuthView person = new PersonAuthView();
        person.setEmail(rs.getString("user_name"));
        person.setPassword(rs.getString("user_password"));
        return person;
    }

    public List<PersonBasicView> getPersonBasicView(){

        try(Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id_person, given_name, surname FROM mydb.person ORDER BY id_person ASC");
            ResultSet rs = preparedStatement.executeQuery();){
            List<PersonBasicView> personBasicViewList = new ArrayList<>();
            while (rs.next()) {
                personBasicViewList.add(mapToPersonBasicView(rs));
            }
            return personBasicViewList;
        } catch (SQLException e){
            throw new DataAccessException("Person basic view could not be loaded", e);
        }

    }

    private PersonBasicView mapToPersonBasicView(ResultSet rs) throws SQLException {

        PersonBasicView pBV = new PersonBasicView();
        pBV.setId(rs.getLong("id_person"));
        pBV.setName(rs.getString("given_name"));
        pBV.setSurname(rs.getString("surname"));
        return pBV;

    }

    public DetailedView findPersonDetailedView(Long personId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT p.id_person, given_name, surname, age, city, email, status_type, s.id_status_type FROM mydb.person p \n" +
                             "JOIN mydb.person_has_address pa ON p.id_person = pa.id_person \n" +
                             "JOIN mydb.address a ON pa.id_address = a.id_address JOIN mydb.contact c ON c.id_person = p.id_person\n" +
                             "JOIN mydb.status s ON s.id_person = p.id_person JOIN mydb.status_type st ON st.id_status_type = s.id_status_type" +
                             " WHERE p.id_person = ?")
        ) {
            preparedStatement.setLong(1, personId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    private DetailedView mapToPersonDetailView(ResultSet rs) throws SQLException {
        DetailedView personDetailView = new DetailedView();
        personDetailView.setId(rs.getLong("id_person"));
        personDetailView.setEmail(rs.getString("email"));
        personDetailView.setName(rs.getString("given_name"));
        personDetailView.setSurname(rs.getString("surname"));
        personDetailView.setResult(rs.getString("status_type"));
        personDetailView.setResultID(rs.getString("id_status_type"));
        personDetailView.setCity(rs.getString("city"));
        personDetailView.setAge(rs.getString("age"));
        return personDetailView;
    }

    public void addPerson(AddPersonView addPersonView){
        String insertPersonIntoPersonTableSQL = "INSERT INTO mydb.person (given_name, surname, age) VALUES (?,?,?)";

        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonIntoPersonTableSQL, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, addPersonView.getName());
            preparedStatement.setString(2, addPersonView.getSurname());
            preparedStatement.setInt(3, addPersonView.getAge());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating person failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }

    }

    public void editPerson(EditView editView) {
        String updatePersonSQL = "UPDATE mydb.person p SET given_name = ?, surname = ?, age = ? WHERE p.id_person = ?";
        String updateContactSQL = "UPDATE mydb.contact c SET email = ? WHERE c.id_person = ?";
        String updateStatusSQL = "UPDATE mydb.status s SET id_status_type = ? WHERE s.id_person = ?";
        String checkIfExists = "SELECT given_name FROM mydb.person p WHERE p.id_person = ?";
        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparePersonUpdateStatement = connection.prepareStatement(updatePersonSQL, Statement.RETURN_GENERATED_KEYS)) {

            preparePersonUpdateStatement.setString(1, editView.getName());
            preparePersonUpdateStatement.setString(2, editView.getSurname());
            preparePersonUpdateStatement.setInt(3, editView.getAge());
            preparePersonUpdateStatement.setLong(4, editView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, editView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This person for edit do not exists.");
                }
                try (PreparedStatement psContactUpdate = connection.prepareStatement(updateContactSQL, Statement.RETURN_GENERATED_KEYS)) {
                    psContactUpdate.setString(1, editView.getEmail());
                    psContactUpdate.setLong(2, editView.getId());
                    psContactUpdate.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("Failed to edit the email.");
                }
                try (PreparedStatement pStatusUpdate = connection.prepareStatement(updateStatusSQL, Statement.RETURN_GENERATED_KEYS)) {
                    pStatusUpdate.setInt(1, editView.getResultID());
                    pStatusUpdate.setLong(2, editView.getId());
                    pStatusUpdate.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("Failed to edit the status.");
                }

                int affectedRows = preparePersonUpdateStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating person failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }

        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }

    }
}
