package org.but.feec.javafx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.javafx.api.DetailedView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DetailedViewController {

    private static final Logger logger = LoggerFactory.getLogger(DetailedViewController.class);


    @FXML
    private TextField idTxtField;
    @FXML
    private TextField nameTxtField;
    @FXML
    private TextField surnameTxtField;
    @FXML
    private TextField ageTxtField;
    @FXML
    private TextField resultTxtField;
    @FXML
    private TextField emailTxtField;
    @FXML
    private TextField cityTxtField;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTxtField.setEditable(false);
        nameTxtField.setEditable(false);
        surnameTxtField.setEditable(false);
        ageTxtField.setEditable(false);
        resultTxtField.setEditable(false);
        emailTxtField.setEditable(false);
        cityTxtField.setEditable(false);

        loadPersonsData();

        logger.info("PersonsDetailViewController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof DetailedView) {
            DetailedView detailedView = (DetailedView) stage.getUserData();
            idTxtField.setText(String.valueOf(detailedView.getId()));
            nameTxtField.setText(detailedView.getName());
            surnameTxtField.setText(detailedView.getSurname());
            ageTxtField.setText(detailedView.getAge());
            resultTxtField.setText(detailedView.getResult());
            emailTxtField.setText(detailedView.getEmail());
            cityTxtField.setText(detailedView.getCity());
        }
    }


}
