package org.but.feec.javafx.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.javafx.api.DetailedView;
import org.but.feec.javafx.api.EditView;
import org.but.feec.javafx.data.PersonRepository;
import org.but.feec.javafx.service.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class EditController {

    private static final Logger logger = LoggerFactory.getLogger(DetailedViewController.class);

    @FXML
    public Button EditConfirmBtn;
    @FXML
    public TextField idTxtField;
    @FXML
    private TextField nameTxtField;
    @FXML
    private TextField surnameTxtField;
    @FXML
    private TextField ageTxtField;
    @FXML
    private TextField emailTxtField;
    @FXML
    private TextField newTestResultIdTxtField;
    @FXML
    private ChoiceBox<String> newTestResultChoiceBox;

    private String[] newTestResultChoices = {"Vaccinated", "Recovered", "Tested positive", "Tested negative", "Hospitalized", "Passed away"};

    public Stage stage;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;

    public void setStage(Stage stage) {this.stage = stage;}

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTxtField, Validator.createEmptyValidator("The id must not be empty."));
        idTxtField.setEditable(false);
        validation.registerValidator(emailTxtField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(nameTxtField, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(surnameTxtField, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(ageTxtField, Validator.createEmptyValidator("The age must not be empty."));
        validation.registerValidator(newTestResultIdTxtField, Validator.createEmptyValidator("The new result id must not be empty."));
        newTestResultIdTxtField.setEditable(false);

        newTestResultChoiceBox.getItems().addAll(newTestResultChoices);
        newTestResultChoiceBox.setOnAction(this::newTestChoice);

        EditConfirmBtn.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("EditController initialized");
    }

    public void newTestChoice(ActionEvent event){
        String newResult = newTestResultChoiceBox.getValue();
        switch (newResult){
            case "Vaccinated":
                newTestResultIdTxtField.setText("1");
                break;
            case "Recovered":
                newTestResultIdTxtField.setText("2");
                break;
            case "Tested positive":
                newTestResultIdTxtField.setText("3");
                break;
            case "Tested negative":
                newTestResultIdTxtField.setText("4");
                break;
            case "Hospitalized":
                newTestResultIdTxtField.setText("5");
                break;
            case "Passed away":
                newTestResultIdTxtField.setText("6");
                break;
        }

    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof DetailedView) {
            DetailedView detailedView = (DetailedView) stage.getUserData();
            idTxtField.setText(String.valueOf(detailedView.getId()));
            emailTxtField.setText(detailedView.getEmail());
            nameTxtField.setText(detailedView.getName());
            surnameTxtField.setText(detailedView.getSurname());
            ageTxtField.setText(detailedView.getAge());
            newTestResultIdTxtField.setText(detailedView.getResultID());
            newTestResultChoiceBox.setValue(detailedView.getResult());
        }
    }

    @FXML
    public void handleEditConfirmBtn(ActionEvent event) {

        EditView editView = new EditView();
        editView.setId(Long.valueOf(idTxtField.getText()));
        editView.setEmail(emailTxtField.getText());
        editView.setName(nameTxtField.getText());
        editView.setSurname(surnameTxtField.getText());
        editView.setAge(Integer.parseInt(ageTxtField.getText()));
        editView.setResultID(Integer.parseInt(newTestResultIdTxtField.getText()));


        personService.editPerson(editView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Edited Confirmation");
        alert.setHeaderText("Your person was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }




}
