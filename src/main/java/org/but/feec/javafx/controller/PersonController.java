package org.but.feec.javafx.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.App;
import org.but.feec.javafx.api.DetailedView;
import org.but.feec.javafx.api.PersonBasicView;
import org.but.feec.javafx.data.PersonRepository;
import org.but.feec.javafx.exception.ExceptionHandler;
import org.but.feec.javafx.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class PersonController {

    private static final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @FXML
    private TableView<PersonBasicView> personTableView;

    @FXML
    private TableColumn<PersonBasicView, Long> columnId;
    @FXML
    private TableColumn<PersonBasicView, String> columnName;
    @FXML
    private TableColumn<PersonBasicView, String> columnSurname;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button addPersonBtn;
    @FXML
    private TextField filterTxtField;

    private PersonRepository personRepository;
    private PersonService personService;



    public PersonController() {

    }

    @FXML
    private void initialize(){
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        columnId.setCellValueFactory(new PropertyValueFactory<PersonBasicView, Long>("id"));
        columnName.setCellValueFactory(new PropertyValueFactory<PersonBasicView,String>("name"));
        columnSurname.setCellValueFactory(new PropertyValueFactory<PersonBasicView, String>("surname"));

        personTableView.getSortOrder().add(columnId);
        ObservableList<PersonBasicView> persons = initializePersonTable();
        personTableView.setItems(persons);

        initializeTableViewSelection();

        logger.info("Application initialized");
    }

    private ObservableList<PersonBasicView> initializePersonTable(){
        List<PersonBasicView> personBasicViewList = personService.getPersonBasicView();
        return FXCollections.observableArrayList(personBasicViewList);
    }

    @FXML
    protected void onRefreshBtnClick(ActionEvent event){
        ObservableList<PersonBasicView> observableList = initializePersonTable();
        personTableView.setItems(observableList);
        personTableView.refresh();
        personTableView.sort();
    }

    public void onAddPersonBtnClick(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/AddNewPersonFXML.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS JavaFX Create Person");
            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit");
        MenuItem detailedView = new MenuItem("Detailed view");

        detailedView.setOnAction((ActionEvent event) -> {
            PersonBasicView personView = personTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/DetailedViewFXML.fxml"));
                Stage stage = new Stage();

                Long personId = personView.getId();
                DetailedView personDetailView = personService.getDetailedView(personId);

                stage.setUserData(personDetailView);
                stage.setTitle("BDS JavaFX Person Detailed View");

                DetailedViewController controller = new DetailedViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        edit.setOnAction((ActionEvent event) -> {
            PersonBasicView personView = personTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/EditDataFXML.fxml"));
                Stage stage = new Stage();

                Long personId = personView.getId();
                DetailedView personDetailView = personService.getDetailedView(personId);
                stage.setUserData(personDetailView);

                stage.setTitle("BDS JavaFX Edit Person");

                EditController controller = new EditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(detailedView);
        menu.getItems().add(edit);
        personTableView.setContextMenu(menu);
    }

    @FXML
    private void FilterButton(){

        ObservableList<PersonBasicView> observablePersonsList = initializePersonTable();
        FilteredList<PersonBasicView> filteredData = new FilteredList<>(observablePersonsList);
        filterTxtField.textProperty().addListener((observable,oldValue,newValue)-> {
            filteredData.setPredicate(personBasicView -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String loverCaseFilter = newValue.toLowerCase();
                if (personBasicView.getName().toLowerCase().indexOf(loverCaseFilter) != -1) {
                    return true;
                } else if (personBasicView.getSurname().toLowerCase().indexOf(loverCaseFilter) != -1) {
                    return true;
                } else if (String.valueOf(personBasicView.getId()).toLowerCase().indexOf(loverCaseFilter) != -1)
                    return true;
                else
                    return false; // Does not match.
            });
        });
        SortedList<PersonBasicView> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(personTableView.comparatorProperty());

        personTableView.setItems(sortedData);
        personTableView.refresh();

    };

}