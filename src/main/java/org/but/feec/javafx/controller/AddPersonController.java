package org.but.feec.javafx.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Duration;
import org.but.feec.javafx.api.AddPersonView;
import org.but.feec.javafx.data.PersonRepository;
import org.but.feec.javafx.service.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class AddPersonController {
    private static final Logger logger = LoggerFactory.getLogger(DetailedViewController.class);

    @FXML
    public Button AddNewPersonConfirmBtn;
    @FXML
    private TextField addNewPersonNameTxtField;
    @FXML
    private TextField addNewPersonSurnameTxtField;
    @FXML
    private TextField addNewPersonAgeTxtField;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;

    public AddPersonController() {
    }

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(addNewPersonNameTxtField, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(addNewPersonSurnameTxtField, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(addNewPersonAgeTxtField, Validator.createEmptyValidator("The age must not be empty."));


        AddNewPersonConfirmBtn.disableProperty().bind(validation.invalidProperty());

        logger.info("AddPersonController initialized");
    }


    @FXML
    public void handleAddNewPersonConfirmBtn(ActionEvent event) {

        AddPersonView addPersonView = new AddPersonView();
        addPersonView.setName(addNewPersonNameTxtField.getText());
        addPersonView.setSurname(addNewPersonSurnameTxtField.getText());
        addPersonView.setAge(Integer.parseInt(addNewPersonAgeTxtField.getText()));



        personService.addPerson(addPersonView);

        personAddNewPersonConfirmationDialog();
    }

    private void personAddNewPersonConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Add Confirmation");
        alert.setHeaderText("A new person has been added to the database.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }




}

