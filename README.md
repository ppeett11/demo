
To build the project enter the following command "mvn clean install" in the project root directory.
To run the project enter "java -jar target/bds-javafx-training-1.0.0.jar" in the project root directory.

Sign-in with the following credentials:
- Username: "user@user.com"
- Password: "userpassword"

To generate the project and external libraries licenses enter the following command in the project root directory
"mvn project-info-reports:dependencies"


The licenses info and documentation will be located in the `target/site` folder.
